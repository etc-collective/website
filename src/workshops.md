---
layout: layouts/portfolio.njk
pagination:
  data: collections.portfolios
  size: 3
  alias: portfolios
---
### Upcoming Workshops

December 3, 4-6pm\
Hosted by OPIRG Peterborough\
751 George Street North Peterborough\
Register: <https://www.eventbrite.ca/e/prison-expansion-workshop-with-etc-tickets-469216588447> \
Facebook Event: <https://m.facebook.com/events/799818171128034>


### Invite us to give a workshop!

Invite us to present this text for your organization, collective, social center, or group of friends! Generally the workshop involves an overview of the information presented in the text, along with some activities and brainstorming about responses to prison expansion. It can be as short as a 10 minute presentation to spread the word about prison expansion during your organization's meeting, or as long as 2-hour interactive workshop. Get in touch at escaping-tomorrow@riseup.net to discuss!
