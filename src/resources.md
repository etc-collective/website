---
layout: layouts/portfolio.njk
pagination:
  data: collections.portfolios
  size: 3
  alias: portfolios
---
We will continue adding additional resources here, so feel free to contact us if you have any suggestions. For now though, here is a list of links to other groups that do related work in the area:

# Links
Toronto Prisoners' Rights Project\
<https://www.torontoprisonersrightsproject.org/>

The Criminalization and Punishment Education Project (Ottawa)\
<https://cp-ep.org/>

Barton Prisoner Solidarity Project (Hamilton)\
<https://www.facebook.com/bartonsolidarityproject>

Prison Dispatches\
<https://www.prisondispatches.net/>

CFRC Prison Radio (Kingston)\
<https://cfrcprisonradio.wordpress.com/>

End the Prison Industrial Complex (Kingston)\
<https://epic.noblogs.org/>

Coalition Against the Proposed Prison (Kemptville)\
<https://www.coalitionagainstproposedprison.ca/>

North Shore Counter-Info\
<https://north-shore.info>
