---
title: "How “accidental activists” may block Doug Ford’s new prison"
des: "Breach Media Article"
date: "2022-04-29"
img: "/assets/images/blog/capp2.png"
btnText: "read more"
author: "Breach Media"
featureImg: "/assets/images/blog/capp.png"
---

### How “accidental activists” may block Doug Ford’s new prison

It only takes five minutes to drive through downtown Kemptville, Ont., a small town 40 minutes south of Ottawa.

Surrounded by farmland and forests, the main street appears straight out of a Hallmark movie. On a spring day in April, a group of neighbourhood children—many wearing festive bunny ears—assemble along the sidewalk, ready for an Easter egg hunt. Every Sunday through the summer, local farmers and artisans line the streets to sell produce, flowers and baked goods. 

Colleen Lynas, a retiree who is one of about 4,000 Kemptville residents, moved here to a newly-built borough four years ago to enjoy her golden years closer to her family.

She was enjoying its peacefulness, until one evening in August 2020 when she received a surprising call from a neighbour. 

Earlier that day, she learned, the Conservative provincial government held a press conference to announce a new plan to revitalize Kemptville’s economy: they were going to build a prison. 

“I was just so shocked,” Lynas remembers. “I don’t think I would have moved here if I knew that that was the masterplan.”

The press conference was the first time she and her neighbours were hearing about it. There had been no mention of it from the mayor and council, or any consultation with community members. The government wasn’t bringing a proposal to residents—the new prison was already a done deal. 

Part of the Ford government’s “Eastern Ontario Strategy” to create jobs and boost the economy, the strategy includes renovating, expanding, and building entirely new prisons across the province.

But Lynas didn’t want to see any more money being poured into corrections, let alone an entire new facility a 10 minute drive from her house. To share her concerns, she began going on walks and bike rides with her neighbours Victor Lachance and Lisette Major.

After their first walk, Lachance in turn called Kemptville’s mayor, Nancy Peckford. According to him, the mayor repeated many of Ford’s talking points: the prison would create 500 jobs and lead to economic growth.

Soon, walks and bike rides among the group became weekly pizza dinners, and their conversations went from inquisitive to strategic. Instead of trying to understand why their mayor and council were supporting the province’s plan without question, they began strategizing how they were going to stop this prison from ever being built. 

Expecting a quieter chapter of their lives, they never planned on running a campaign. But by September of 2020, the trio of retirees had formed the Coalition Against the Proposed Prison. Over the course of a year and a half, this group of local community members—”accidental activists,” as they were eventually called—would grow, building a formidable campaign with enough support to seriously threaten the Ontario government’s plans for a new prison.  

#### The making of accidental activists

“We didn’t know squat about the prison system,” says Lachance, recalling the early days of their coalition against the project.

These days, most of the members of the coalition against the prison are retirees or working nine to five jobs and raising kids. Had a prison not been forced onto their town, chances are they never would’ve gotten involved in anti-prison politics. 

But they did have some preparation. Lachance was involved in the fight to end capital punishment in the 1970s, while Lynas had a long career as a social worker and on occasion had worked with prisoners. But before Ford’s announcement, she had never thought much about prisons or incarceration. 

Now, if they were going to put up a serious fight against the province and their local officials, they would need to learn the institution they were up against inside and out.

The coalition decided to reach out to the Ottawa-based Criminalization and Punishment Education Project for information.

Justin Piché, a member of the group and Associate Professor in the Department of Criminology at the University of Ottawa, was the one who responded to their Facebook message. 

From Piché, the Kemptville group learned that their fight didn’t start with Ford’s announcement. 

It started back in 2016, when the Ottawa-Carleton Detention Centre was making headlines for triple-bunking people in cells built for one and housing prisoners in the segregation unit showers. The headlines spurred the creation of a task force that would look into solutions to crowding and the treatment of incarcerated people. 

The task force succeeded in getting the Minister of Corrections Yasir Naqvi to make bail reforms that resulted in some people being released from prisons. As they were pushing for more systemic changes, Naqvi was replaced and the Liberals introduced a twist. They would replace the 585-bed Ottawa-Carleton Detention Centre with a new billion-dollar 725-bed Ottawa Correctional Complex—using the push for reforms to actually expand the prison system.  

By the time the election rolled around, talk of building a new prison waned—that is, until Ford’s announcement in August 2020 introduced yet another twist. 

“Rather than putting 725 new beds in Ottawa, they’ve spread it across Eastern Ontario,” Piché explains. He says dispersing the beds has made the plan more difficult to fight against, because the government is now building in three areas instead of just one. 

If the government had made their fight more challenging, it would need more unlikely alliances.

The Criminalization and Punishment Education Project had never organized outside of Ottawa’s downtown core. A rural conservative stronghold like Kemptville was uncharted territory. They didn’t much know the area. And to top it off, they believe strongly in the abolition of all sites of incarceration anywhere, whereas many of the people organizing in Kemptville just didn’t want a prison in their town. 

But if people in Kemptville were interested in struggling against Ford’s plan, they would work with them no matter their reasons.

#### A town built on agriculture, not incarceration

“They were not community organizers when this started,” Piché said. “But they became among the more effective groups of community organizers I’ve had the pleasure to work with.”

Lynas, Lachance and the rest of the coalition—whom Piché calls “accidental activists”—began their campaign by learning about the realities of the prison system: the mass-incarceration of Black and Indigenous people, the disabling nature of prison, and the economic cost of incarceration. 

With the government presenting the prison as a gift for the town, priority number one was to get the facts about the proposed prison and the larger system of incarceration to residents.

Within a month, the coalition against the prison created a website and began planning their first event, a public information session. Their plan was to invite locals to share their point of view and criminalization experts to debunk the myths around “safety,” “job creation” and “economic growth” that their political representatives kept repeating.

In the following six months, they hosted two more information sessions and a handful of rallies. They also contributed regularly to their local paper, the North Grenville Times, and sent open letters to their MPP Steve Clark expressing their concerns.

And instead of a campaign solely focused on stopping something, the coalition wanted to tap into residents’ visions for the future of the town.

“There’s already a vision for this community and it doesn’t involve a large prison on a piece of farmland,” Lynas says. “Never was a prison in the plan.”

The site where the prison is to be built is part of the community’s former agriculture college campus. Since the college’s closure in 2015, its served as an educational hub primarily used by local school boards.

Residents and local officials had hoped the farmland would become a hub of local food production, or perhaps one day, there would be enough funding for a new agriculture college. Since Ford announced the prison, Mayor Peckford had been adamant that the plans to return the farmland to local food production and education could still move forward—alongside the prison. 

“It’s my assumption that there’s a way to insulate the prison from the community visually… and still do interesting things on the campus side,” Peckford stated during a council meeting a few months after the prison was announced. 

Today, Lynas rolls her eyes while recalling the mayor’s line.

In June of 2021, she received an interesting email from a Kemptville resident. The resident had filed a Freedom of Information request for documents related to the site selection for the proposed prison, and they had made a fascinating discovery. 

The documents revealed that Kemptville didn’t meet the government’s own criteria for prison site selection. It’s more than 40 km from Ottawa, has no public transportation, and lacks community support services for prisoners and their families. 

The document also stated: “The parcel at 178 acres meets the ministry’s size constraints with potential future expansion if necessary.” 

Not only had the mayor and council kept up their line about local agricultural use, but they’d even been hosting public engagement sessions to get resident input on the future use of the property.

Lynas says the provincial government would never admit to residents that their plan is to begin with a 235-bed prison, and then expand over time—but now they had their proof. 

As the campaign built momentum, Kemptville local Mitch Bloom conducted community polls he’d started when the prison plan was first announced. Each survey found that over half of residents were opposed to the prison, just over a third were in support, and about 10 per cent were unsure. However, with more than 400 responses, the most recent survey had nearly double the respondents of the first, meaning the coalition’s campaign may have encouraged some residents to pay attention and take a stand. 

But Lynas says it’s difficult for a lot of people to make their stance public. “There’s a lot of quiet support for us,” she says. “I think it’s been hard for the business community to say a lot because the municipality is bidding on the site. And because it’s a small town, they don’t want to alienate customers.”

#### “Save don’t pave”

The coalition decided early-on that since they lived in Kemptville and understood the local context, the Criminalization and Punishment Education Project, even though more experienced, would act like their advisors, providing them with organizing materials and educational information.  

Piché recalls a meeting when they were discussing messaging, and he suggested they use the classic prison abolitionist slogan “build communities not cages.” The coalition said no, because they had a better idea.

At a public engagement session the previous fall, the Ministry of the Solicitor General accidentally let it slip that the farmland had not been transferred to the province yet—and it was still under the purview of the Ministry of Agriculture. 

After learning this, the coalition decided saving the farmland should be front and center of their messaging. “Save don’t pave” became their new slogan. 

They spread the word that they were trying to bridge the gap between food justice and anti-prison organizing. Soon enough, Amanda Wilson got in touch. Wilson is an organizer and an Assistant Professor at Saint-Paul University working at the intersection of food justice and prison abolition. Before becoming an academic, she worked at a number of food justice organizations that didn’t necessarily consider prison justice as a part of their movement. 

After seeing the coalition’s call for the food movement to get involved, Wilson mobilized her networks to try and bridge the gap between these two movements. She wrote an op-ed for the Ottawa Citizen about saving the farmland. She convinced dozens of food scholars from across the country to sign an open letter to the Minister of Agriculture Lisa Thompson calling on Thompson to refuse to transfer the land to the Ministry of the Solicitor General. 

“If you’re against the prison for environmental reasons, you know, how can we work together?” she says. “Is there enough commonality that we can move the needle forward? 

To Wilson, this wasn’t just an opportunity to get the coalition more support for their campaign, but also to expose her food movement networks to new politics. “It’s easier to have those conversations because you already know them, and you know their politics, you know their starting points,” she said. “And so you build those relationships and that becomes an entry point maybe for more organizational connections to build and develop.”

The collaboration between the coalition, the Criminalization and Punishment Education Project and food movement activists like Amanda Wilson has been key for the success of the campaign. The coalition knows the local context and has the ability to reach people where they’re at; the project has intimate knowledge of the prison industrial complex and how to intervene in government processes; and food movement organizers like Wilson are able to mobilize people and organizations that otherwise might not be aware that a new prison is being built.

The organizing has been paying off. In recent months, the candidates for the opposition parties provincially have all come out against the prison. If elected, the Greens and New Democrats say they would put an end to the project, and the Liberals say they would place a moratorium on its construction.

“You don’t have to be an abolitionist to work together to accomplish abolitionist goals,” Piché says. “We actually do need to go into conservative communities and organize. I think we have to go into places that are deeply uncomfortable to work in sometimes, and where the rhetoric isn’t familiar or perfect.”

Lachance says that before the coalition’s work, “If somebody asked me, ‘do we need prisons?’ I would have said ‘Yeah of course, many prisons.’” 

But after a year and a half of working on this campaign, his opinions have changed. “I’ve learned a lot about the criminal justice system. I’m not an abolitionist, but I might become one, based on everything I’ve learned.”

[Link to original article](https://breachmedia.ca/how-accidental-activists-may-block-doug-fords-new-prison/)
