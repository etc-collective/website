---
title: "Contractor selected for new Thunder Bay correctional complex"
des: "News article from TBNewsWatch"
date: "2022-04-04"
img: "/assets/images/blog/tbcc-rendering-sm.png"
btnText: "read more"
author: "TBNewsWatch"
featureImg: "/assets/images/blog/tbcc-rendering.png"
---

### Contractor selected for new Thunder Bay correctional complex

THUNDER BAY — The Ontario government has selected EllisDon to build and maintain a new 345-bed multi-purpose correctional complex in Thunder Bay.

Infrastructure Ontario is now working to finalize contract details of the public-private partnership with the company.

Although the start and completion dates for the project will be confirmed in the coming weeks, a spokesperson for correctional officers said Monday he believes construction will start by late summer.

The contract cost has not yet been announced but a government estimate last year said it will range between $500 million and $1 billion.

Thunder Bay Regional Health Sciences Centre, completed in 2004, was built at a cost of $284 million.

The correctional complex will replace both the nearly-century-old district jail and the Thunder Bay Correctional Centre on Highway 61.

EllisDon will design, build, finance and maintain it.

The district jail on McDougall Street is frequently crowded well beyond its designed capacity.

After touring the facility in 2019  Ontario Ombudsman Paul Dubé called conditions in the jail the "most disturbing thing" he'd seen in his four years of service.

Dubé cited instances of four inmates being placed in cells meant for two people.

Plans to replace the district jail were first announced by the government of former Premier Kathleen Wynne in 2017.

It's been four years since a request for qualifications from interested contractors was issued.

The short list of two successful applicants wasn't released until three years later, in March 2021.

Shawn Bradshaw, head of the OPSEU local at the correctional centre, said the change in government in 2018 and the COVID-19 pandemic combined to slow the process, but he's happy to see the project approaching the sod-turning stage

[Link to original article](https://www.tbnewswatch.com/local-news/contractor-selected-for-new-thunder-bay-correctional-complex-5229370)
