---
title: "Fall News Roundup"
des: "Project updates as of fall 2022"
date: "2022-10-30"
img: "/assets/images/blog/TBCC2.png"
btnText: "read more"
author: "ETC"
featureImg: "/assets/images/blog/TBCC.png"
---

### Project Updates as of October 2022

Here's a brief round up of updates to a few of the projects, both expansions and new sites -

#### Expansions in Thunder Bay Correctional Center and Kenora Jail

As of September 22, 2022, Infrastructure Ontario and the Ministry of the Solicitor General announced that construction of the Thunder Bay Correctional Centre and Kenora Jail expansion projects has reached substantial performance. This means that the Ministry of the Solicitor General has access to the spaces in order to prepare them for occupancy.

[https://infrastructureontario.ca/Substantial-Performance-Reached-Thunder-Bay-Correctional-Centre-and-Kenora-Jail/](https://infrastructureontario.ca/Substantial-Performance-Reached-Thunder-Bay-Correctional-Centre-and-Kenora-Jail/)

#### Thunder Bay Correctional Complex

The contract for this new facility was awarded to Ellis Don Justice on April 22, 2022. The contract is for 1.2 billion dollars to design, build, finance, and maintain the structure for 30 years. There will be 7-800 workers involved at the Highway 61 site, where most of the construction will be happening since this particular project is not a modular build. On site construction began this summer 2022, and is slated to be complete in Fall 2026.

[https://infrastructureontario.ca/Contract-Awarded-Thunder-Bay-Correctional-Complex/](https://infrastructureontario.ca/Contract-Awarded-Thunder-Bay-Correctional-Complex/)

[https://www.tbnewswatch.com/local-news/building-the-new-thunder-bay-jail-will-require-700-800-workers-5599599](https://www.tbnewswatch.com/local-news/building-the-new-thunder-bay-jail-will-require-700-800-workers-5599599)

#### Eastern Ontario Correctional Complex

We have been keeping our eyes on this project especially because the province's published timeline for this had the RFQ going out this past spring/summer, but according to our research, it appears the RFQ has still not gone out.

[https://www.infrastructureontario.ca/uploadedFiles/_CONTENT/News/2_Market_Update/May_2022_Market_Update.pdf](https://www.infrastructureontario.ca/uploadedFiles/_CONTENT/News/2_Market_Update/May_2022_Market_Update.pdf)

The Coalition Against the Proposed Prison in Kemptville (CAPP Kemptville) filed an Application for Judicial Review against the proposed EOCC build. Essentially this means that they are asking the Divisional Court to assess this proposal for legality. Their website includes a backgrounder and a link to their press conference that further explains their reasons for submitting this application.

[https://www.coalitionagainstproposedprison.ca/news-events](https://www.infrastructureontario.ca/uploadedFiles/_CONTENT/News/2_Market_Update/May_2022_Market_Update.pdf)

