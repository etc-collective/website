---
title: "“Proposed provincial prison in Kemptville is illegal”, Notice of Application for Judicial Review Filed"
des: "Press Release from the Coalition Against the Proposed Prison"
date: "2022-08-15"
img: "/assets/images/blog/eocc2.png"
btnText: "read more"
author: "Coalition Against the Propsed Prison"
featureImg: "/assets/images/blog/eocc.png"
---

### “Proposed provincial prison in Kemptville is illegal”, Notice of Application for Judicial Review Filed

15 August 2022 (Kemptville, Ontario) – Two residents of the small, rural town of Kemptville have filed a Notice of Application with the Divisional Court of Ontario for a Judicial Review of the Ontario government’s proposed plan to pave over prime farmland and bulldoze existing farm buildings located on the former Kemptville Agricultural College to build a provincial prison. Victor Lachance, a member of the Coalition Against the Proposed Prison (CAPP), and Kirk Albert, a member of the Jail Opposition Group (JOG), are doing so in the interest of many residents, community organisations and agri-food groups who have been frustrated by the province’s refusal to meaningfully consult with and provide adequate information to North Grenville residents about their prison plan.

Stéphane Émard-Chabot of the law firm Sicotte Guilbault, acting on behalf of Lachance and Albert, notes: “Through our legal research, our firm has determined that when the provincial government announced in August 2020 that it planned to build a correctional complex on farmland of the former Kemptville Agricultural College they broke the law by not abiding by its own Provincial Policy Statement and the local Official Plan, as it is obliged to do under the Planning Act”. He adds: “The Judicial Review launched today by my clients is challenging the legality of the decision and is seeking a Prohibition Order that will save the farmland and several farm buildings located on the property by stopping the construction of the proposed prison”.

“Ontario is losing over 300 acres of farmland a day”, says Victor Lachance. “How can the province justify its cavalier approach of destroying farmland and farm buildings for a prison that nobody asked for and that experts argue we do not need?” He adds, “Up to $499 million has been earmarked for a 30-year public-private-partnership to design, build, finance and maintain the proposed prison. The contract for this P3 has not been signed so these funds should be used to help farmers and their communities, not to build cages for people who have been pushed to the margins of our society and are seeking support and care instead of imprisonment”.

“For two years, residents of Kemptville have been requesting details about the proposed prison from the province through correspondence and Freedom of Information requests about its site selection and due diligence activities, including the environmental assessment. The province has refused to share substantive information about its process or its decision to pave over valuable farmland in Kemptville for a prison”, says Albert. He adds, “From many perspectives, the province has failed in its commitments to our local government and residents since announcing its plan in August 2020 and we hope the North Grenville Municipal Council will support this Judicial Review to save not only the land, but the Green and Growing vision for Kemptville and North Grenville’s future, that they themselves championed for the community”.

The Judicial Review applicants are calling on the Ontario government to look at the grounds for the application and agree that they should not proceed with the proposed prison. “It’s not too late to change course on their prison plan”, states Lachance. A summary of why the applicants are arguing that the proposed provincial prison in Kemptville is illegal is attached. For more information about the proposed provincial prison in Kemptville, please visit [www.cappkemptville.ca](www.cappkemptville.ca). 

