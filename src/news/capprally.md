---
title: "Local Residents Hold Rally Ahead of North Grenville Public Consultation Session"
des: "Press Release from the Coalition Against the Proposed Prison"
date: "2022-04-11"
img: "/assets/images/blog/capp-rally2.png"
btnText: "read more"
author: "Coalition Against the Propsed Prison"
featureImg: "/assets/images/blog/capp-rally.PNG"
---

### Press Release: Local Residents to Hold Protest Rally Ahead of North Grenville Session on the Future of ‘Surplus’ Farmland Tied to the Proposed Provincial Prison

April 11, 2022 (Kemptville, ON) – Ahead of the session organized by North Grenville’s mayor and council on the “Future Use of Surplus Farmside Lands” taking place on April 12th at 6:00 p.m., and linked to the province’s proposed prison, residents of Kemptville will hold a protest rally also on April 12th beginning at 5:15pm in front of the W.B. George Centre (48 Shearer Street, Kemptville Campus). Those gathered at the rally will demand that municipal officials cease their support for the province’s proposed prison and instead recommit to efforts to acquire the entire 182-acre site located on the former farmside of Kemptville College.

Both the municipal government session and resident rally are taking place at a time when the provincial government and local MPP Steve Clark have faced significant, sustained, and growing opposition to the proposed 235-bed prison. It is in this context that the Ministry of the Solicitor General (SolGen) offered up a portion of the property for local uses as an inducement to pave over prime farmland and bulldoze existing farm buildings to build a prison no one in the community asked for and many do not want. “This so-called community consultation to talk about how the municipality will use the land confirms that Mayor Peckford and the rest of North Grenville Council have accepted the province’s inducement. It appears they’re more interested in advancing an agenda cooked-up at Queen’s Park than fighting for their own constituents and the future we want to build,” says Victor Lachance, member of the Coalition Against the Proposed Prison (CAPP). 

“The timing of the session is very telling. Heading into a provincial election, all the other provincial parties have spoken out against paving over this farmland, calling for a moratorium on the proposed prison in order to conduct meaningful consultation with the community prior to spending hundreds of millions of dollars on an unnecessary prison. This is something we’ve been asking for since the proposed prison was first announced. This appears to be a blatant effort by the municipality to mitigate the sustained and building pressure being put on the province and local MPP Steve Clark leading up to an election”, says Kirk Albert, Jail Opposition Group spokesperson.

Colleen Lynas, spokesperson for CAPP, adds: “How can the municipality be holding this session when they haven’t solved the problem of taxpayers having to pay for additional costs for this prison, like increased policing expenditures? How is it responsible to put this session on when the province hasn’t completed or disclosed information about the due diligence activities at the site where they want to erect the prison, and the farmland they want to pave over for the project hasn’t even been transferred from the Ministry of Agriculture to SolGen? The timing of this session appears to be an attempt to help MPP Steve Clark greenwash imprisonment and this bad deal for his constituents. The municipality should immediately refuse to accept the province’s inducements and call for a moratorium on the prison proposal in order to hold robust public consultations on how to best use the farmland.”

For more information or for media interviews:
Coalition Against the Proposed Prison contact Victor Lachance @ cappkemptville@gmail.com; 613-266-0264
Jail Opposition Group contact Kirk Albert @ jail.opposition.group@gmail.com; 613-327-8268

[Visit the Coalition Against the Proposed Prison website](https://www.coalitionagainstproposedprison.ca/)
