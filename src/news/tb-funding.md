---
title: "Thunder Bay correctional facility to get $1.2B in Ontario funding amid flurry of spending promises"
des: "CBC News Article"
date: "2022-04-28"
img: "/assets/images/blog/doug-ford2.png"
btnText: "read more"
author: "CBC News"
featureImg: "/assets/images/blog/doug-ford.png"
---

### Thunder Bay correctional facility to get $1.2B in Ontario funding amid flurry of spending promises

As part of a flurry of Ontario government spending promises as the election approaches, the province has promised $1.2 billion to the City of Thunder Bay for a long-awaited correctional facility.

The funding was announced this week, ahead of Thursday's provincial budget, as part of $10 billion in spending promises that Premier Doug Ford and his ministers have been rolling out since March. 

The planned 345-bed Thunder Bay Correctional Complex is targeted to replace the Thunder Bay Jail, which is nearly a century old, and the Thunder Bay Correctional Centre.

Construction is slated to begin this fall and will take about four years, the province said in a news release earlier this week. 

EllisDon was earlier awarded the contract to design, build, finance, and maintain the facility, which the government said will include behavioural and mental-health housing, improved programming, and better access to natural light and outdoor spaces.

Bill Hayes is president of Ontario Public Sector Employees Union's Local 737, which represents employees at the Thunder Bay Jail.

Hayes said the correctional complex announcement is "huge."

"A lot of us have been waiting many, many years for that announcement," he said. "Up until now, it's just been a hope and a dream.

"It's nice to see that it's finally here, and I think people are very positive for the future."

The Thunder Bay Jail, which also serves as the region's remand centre, has a history of dealing with overcrowding, and the facility wasn't designed to accommodate programming for inmates, Hayes said.

It has an official operational capacity of 124 inmates.

But Hayes referred to the jail as, essentially, a facility for "housing bodies."

"We've had overcrowding for years, and all we've been doing is basically trying to find locations to house inmates throughout the institution, whether that be visiting rooms, interview booths or our admitting-discharge area," he said.

"We just don't have the room, and never mind the time to do any programming or anything like that.

"You're dealing with close to 200 inmates in that small facility," he said. "That's all you can do. That's all you can manage. One little thing throws the whole entire institution off, and we're scrounging for staff, and staff are getting burnt out."

Hayes said the planned new complex — jail employees had input into its design — will help mitigate those issues.

"It's not going to fight us. It's not going to go against us like the Thunder Bay Jail does right now," he said. 

"This building will be made for fresh air every day, for programming every day. We can take care of all the needs of all the inmates and not just a certain select few of them."
Retail politics

Between early March and April 20, the province has announced over $10 billion in spending, rebates and tax cuts, according to a CBC News analysis.

In recent weeks, barely a day has passed without a multimillion-dollar announcement from Ford or one of his ministers. 

And things haven't slowed down:

*On April 27, Ontario announced more than $87 million over three years to help the Toronto Police Service address gangs-and-gun violence. Another $10 million was announced for a new health-care centre in Timmins the same day.
*On April 26, more than $38 million was announced for the construction of a new patient tower at Toronto Western Hospital, as was $5.5 million to boost Ontario's auto-parts supply chain.
*On April 25, $1 billion was said to be going to expand home care for seniors and recovering medical patients in the province.

All announcements are posted on the province's news page.

These kinds of announcements close to an election aren't new, said Lydia Miljan, professor of political science at the University of Windsor in southwestern Ontario.

"This is a standard playbook," she said. "Governments typically will do a lot of spending announcements, and we've seen them for the last several months just gearing up toward the election campaign, reminding people why they voted for them the first time and hope that they will be spurred to continue their support."

The next provincial election is set for June 2. Exactly what effect funding announcements have on voters, however, is more difficult to track.

"It is hard to know what exactly people vote for," Miljan said. "People obviously vote for policies that they approve of, but ... our politics tends to emphasize personalities and leadership."

The correctional complex was first announced by the previous Ontario Liberal government in 2017. The PCs formed a majority in the subsequent provincial election, and re-announced the project in 2019.

And while pre-election polls suggest the PC majority — as of Wednesday, poll aggregator 338Canada suggested there is  about a 70 per cent chance of that happening — another party forming the government won't necessarily mean the correctional complex won't go ahead as planned.

"If the community has been wanting this facility for a long time, and they get a firm promise by a current government, that may be enough to sway another party to say, 'Well, we better stick with with that," Miljan said.

However, there are no guarantees.

"We know that when new governments come in, they do have different priorities, and they usually open the books and go, 'Oh dear, the books aren't anything like we thought they were, and we can't do all the things that the other government promised,'" she said. "People know that that's a risk, that spending announcements made prior to an election may not necessarily come to fruition."

Hayes, for one, is optimistic the project will go ahead no matter what happens on June 2.

"I don't see how anybody cannot support it," he said. "It's desperately needed."

"I feel like it has to move forward," Hayes added. "I know the price tag seems big, but in the long run, I think it's going to be beneficial to Thunder Bay and the whole northwest altogether."

[Link to original article](https://www.cbc.ca/news/canada/thunder-bay/correctional-complex-update-1.6432483)
