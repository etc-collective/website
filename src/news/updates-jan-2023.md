---
title: "Winter 2023 News Roundup"
des: "Project Updates as of January 2023"
date: "2023-01-25"
img: "/assets/images/blog/complex-construction-aerial-sm.png"
btnText: "read more"
author: "ETC"
featureImg: "/assets/images/blog/complex-construction-aerial.png"
---

### Project Updates as of January 2023

Since our last update, EllisDon has started work on the new prison in Thunder Bay, the so-called Thunder Bay Correctional Complex (TBCC). They started work in early November, and the pictures in this post show the status of the site.

Of course, this feels terrible to everyone who understands prison to be an insult to the human spirit, but work won't be complete until 2026. There is also quite a bit of new info available that could be of interest to those of us who do not intend to just let these prisons get built.

As we anticipated, the number of prisoners the government claims this prison will hold has started rising. On Infrastructure Ontario, they are now stating the capacity as 345, which is up from 325 as recently as a couple of months ago. This is to be expected because the initial number was lower than the prisons they were claiming to replace, and because that is just how these projects tend to go.

In addition to that increase, EllisDon and the province revealed that the recently completed expansion to the Thunder Bay Correctional Centre (on the same site as the TBCC) is in fact permanent and will be connected to the TBCC. They are calling it the Modular Build Facility, or MBF, and it is located south-east of the TBCC site. The stated capacity of the MBF is 50 people, which is again higher than the initial number, since the province first claimed the expansion to the Kenora Jail and the Thunder Bay Correctional Centre would be for a combined 50 people.

[https://www.ellisdon.com/projects/thunder-bay-correctional-complex](https://www.ellisdon.com/projects/thunder-bay-correctional-complex)
[https://www.infrastructureontario.ca/Thunder-Bay-Correctional-Centre/](https://www.infrastructureontario.ca/Thunder-Bay-Correctional-Centre/)

With this supposedly temporary capacity now being permanent and with space for 20 more people, that makes for a total of 70 more beds than were initially announced, an increase of 15%. As we have said before, we will only believe they are actually closing the old prisons when they are actually demolished, and this kind of maneuver is exactly why.

Here are a few more details about the project:

EllisDon has a contract to continue maintaining the prison for 30 years. The contract includes the construction of a 4000 square foot wastewater facility (so maybe we can assume the prison won't share the city water system?). The contract with EllisDon is worth 1.45 billion dollars (about triple what was estimated when the project was announced) and [they expect to employ over 700 workers](https://www.northernontariobusiness.com/industry-news/design-build/building-the-new-thunder-bay-jail-will-require-700-800-workers-5612991).

Interestingly, the TBCC is not a modular build, so most of the work will be done on site. So EllisDon will only be making a little use of their [modular construction plant in Hamilton](https://www.ellisdon.com/news/hamilton-plant-aims-to-make-modular-construction-mainstream) (EDmodular, located on South Service Road between Fruitland Road and Fifty Road).

The TBCC is being designed by [Zeidler Architecture Inc. and DLR Group Inc](https://www.on-sitemag.com/construction/contract-awarded-for-thunder-bay-correctional-complex-project/1003976894/). These are both huge, transnational firms, but Zeidler is based here in Ontario. Their Toronto office is unit 600 at 158 Sterling Road, and the person in charge of this project is David Collins.

The TBCC and the modular builds are the latest volley in a series of new prison construction in the north and east of the province. After the TBCC, the next project to get underway will be the Kemptville prison. Getting to know the players and the process now will be useful in the future, and we are in this for the long haul.

Top image: aerial view of construction on the Thunder Bay Correctional Complex

<figure>
<img 
src = "/assets/images/entrance-complex.png"
alt = "Entrance to construction site of the Thunder Bay Correctional Complex">
<figcaption align = "center">Entrance to construction site of the Thunder Bay Correctional Complex</figcaption>
</figure>

<figure>
<img 
src = "/assets/images/aerial-center.png"
alt = "Aerial view of the Thunder Bay Correctional Centre">
<figcaption align = "center">Aerial view of the Thunder Bay Correctional Centre</figcaption>

</figure>
