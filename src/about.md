---
layout: layouts/about.njk
des: "We are a group of friends who live in Southern Ontario and who have, separately and together, been doing prisoner solidarity organizing of different kinds for many years. We are anarchists, and although our focus has often been prison, we bring to it a broader critique of power and hierarchy that influences how we choose to organize. Some of us have done time, but all of us know what it's like to be separated from people we care about by prison walls, and this feeling is a big part of why we don't want to see the system expand. You can get in touch with us at escaping-tomorrow@riseup.net."
---
# Instead of describing ourselves further, here are some quotes from texts that inspire our thinking about the struggle against prison:<br>
