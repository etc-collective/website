---
subTitle: "" 
title: "A Strategy for Stopping Prison Expansion"
img: "/assets/images/projects/icon5.jpg"
linkText: "Read more"
---
Our main goal in writing this text and drawing attention to the issue of prison expansion in Ontario is to stop the projects, so in this final section we would like to offer some ideas about how that could happen. This is just our contribution to what we hope will be a larger conversation, and we certainly aren't trying to say there is only one way of struggling against prison. That said, we hope that you will resonate with our ideas and feel inspired to push in similar directions.

First though, who is this "we"? We are a group of friends who live in Southern Ontario and who have, separately and together, been doing prisoner solidarity organizing of different kinds for many years. We are anarchists, and although our focus has often been prison, we bring to it a broader critique of power and hierarchy that influences how we choose to organize. Some of us have done time, but all of us know what it's like to be separated from people we care about by prison walls, and this feeling is a big part of why we don't want to see the system expand.

In this section, we are speaking to two overlapping groups of people: those who do prisoner support and solidarity, and those who share our politics as anti-capitalists and anti-authoritarians. For those who share our broad politics, we want to encourage you to care about the details of prison expansion in Ontario and put energy into stopping it. For those who already do work around prison, we want to make the case for a certain way of thinking about how and why to oppose prison expansion.

We will talk first about the opportunities we see in building a campaign against prison expansion, and then present some ideas for how to go about it.

### Opportunities

##### Regional organizing

Although each individual construction project is local, the overall process of prison expansion is regional and provincial in scale. The prison system affects all of us, no matter where live, and although there are specific reasons why people in and around Thunder Bay or Kemptville might want to oppose the projects in their communities, every single person in (or near) Ontario has a reason to oppose both the expansion overall and each individual project.

The financial and political interests around the expansion projects certainly aren't limited to the communities where the prisons are located. Framing the issue as provincial in scale creates opportunities to network more widely and also to take the struggle to the doorstep of every construction company, OPSEU office, politician, or architecture firm that is driving prison expansion forward.

##### Set our own time frames, prepare for the future

Often, our organizing is reactive and urgent, meaning we are responding to things happening around us as quickly as we can. This is definitely not just true of organizing around prison. In this case though, the expansion project will be happening over at least the next three years. There will be moments where things are urgent, but generally speaking, a campaign against prison expansion could set its own time frame and have control over the pace of struggle.

Working on longer time frames also makes it easier to look beyond the projects currently on the table and prepare for what will come after. The Eastern and Northern Strategies are not the first waves of prison expansion and won't be the last (see our case study about the Toronto South Detention Centre). Stopping the current projects is not our only goal. How we organize in the present creates the conditions we will fight in next time around.

##### Deeper Analysis of Prison in Ontario

Anyone who organizes around prison knows that one of the biggest hurdles is how little visibility the issue of prison has. Canadian society as a whole just kinda chooses not to pay any attention. In politicized spaces, although many groups have some general desire for prison abolition, they often don't know much more than the average person about how prison in Canada actually works.

Ontario's prison system certainly has similarities to prison systems elsewhere, but it is also specific to this region. One of the best ways to learn about the prison system is to support prisoner organizing, since the tiny routine details of how the system works are the terrain on which that struggle occurs.

Understanding how prison works in our context can help us understand how power functions more generally and help give teeth to an abolitionist politic, since when we struggle against prison, we struggle against state violence at its most bare.

A campaign against expansion is also an opportunity for folks who already do prisoner solidarity work, and here's the main reason why:

##### Struggle against recuperation

As we explained in a [previous section](https://escapingtomorrowscages.org/texts/3recuperation/), recuperation is when the state tries to involve its critics in a process of transforming the institutions they criticize. The best way for the state to undermine prison organizing is to take up a version of our demands and use it to justify expansion and reform.

One of the strengths of the current prisoner solidarity movement in Ontario is also a vulnerability: focusing on our local prisons and amplifying the demands prisoners make about their conditions inside. This is a key part of why this organizing has been so effective in recent years, but it also makes it harder for us to react when the prison system responds to our demands by "improving" itself in a way that goes beyond any individual prison.

We have done a great job rallying people around demands that come from inside prison about conditions there. However, on the inside, a conflict over triple bunking or early lockup can easily escalate to become a fundamental challenge to the prison's authority. Prisoners in struggle change the balance of power inside. On the outside though, the same demands that are so powerful inside are actually just mild policy disagreements. Therefore, amplifying prisoner demands may not be a sufficient basis for an abolitionist politic on the outside.

A specific campaign against Ontario prison expansion can happen in parallel with support for inside organizing. It can help to broaden the issue and give us space to argue against the existence of prisons, and it also allows us to push back against the prison system's attempts at undermining prison struggle through recuperation.

### Strategy

We want to propose a two-pronged strategy for stopping this wave of prison expansion: direct action targeting the construction process, and undercutting its social and political support.

Obviously we aren't inventing anything here. These dual approaches are commonly used in other campaigns. For instance, in campaigns against pipelines in Ontario, people occupied construction sites and also undermined pro-pipeline advocacy in order to deprive the project of support. Around encampment defense, people helped to physically prevent evictions while also targeting politicians and lobby groups who supported evictions.

Although these two approaches are distinct, it is important to think of them as part of a single strategy. Different crews may do different kinds of work, but we should emphasize the way those kinds of work complement each other. We will look at each approach separately, but first we want to propose a general principle:

**Decentralization** means encouraging organizing within many different, autonomous groups, whether ones that already exist or ones formed for this purpose. Decentralization is an alternative to centralization, which would look like trying to funnel everyone who wants to oppose prison expansion into one big organization.

Decentralization allows for a wider diversity of approaches and messaging, making the campaign more creative and wide-reaching, and it makes it easier to scale up across different regions. It definitely comes with challenges, like for instance, coordinating between different groups can take more work and it can be harder to build momentum in some cases. But for us, the advantages outweigh the inconveniences.

We aren't going to be trying to get anyone to join anything -- rather, we will be encouraging folks to self-organize within as many different groups as possible. Coordination is still important, so we encourage you to send content about the expansion to (name of website) and action reports to North Shore Counter-Info, which is a secure, autonomous media platform.

OK, on to the two approaches:

##### Undercutting Support for Expansion

Right now, there is a whole network of support for prison expansion. This often looks like support for prison reform, notably around mental health, addiction services, and programming for Indigenous prisoners. Many of the groups and individuals doing this are not our enemies, but they are participating in the province's recuperation strategy, and not all the groups support it to the same degree.

Undercutting support for the expansion looks like like:

1. Dramatize the issue of expansion. Push the conflict into the open – break social peace and make it clear that there are sides in this struggle;
2. Giving the less supportive groups the chance to step away from the project and withdraw their support;
3. Isolating the most hardened groups that are unlikely to step away and reveal them as enemies. The activities of these hardened groups can then be targeted for disruption.

Below is an incomplete list of some groups that currently support this project. They are approximately listed in order of how supportive they are of the prison system, from less supportive to more. Those at the top can be asked (or pressured) to drop their support in order to isolate those at the bottom.

* Various mental health organizations (Schizophrenia Society of Ontario, CMHA)
* The Assembly of First Nations\
  *Grand Chief Alvin Fiddler: "These are much-needed improvements and we acknowledge the Solicitor General's commitment to improving living conditions and providing more culturally relevant and inclusive supports for inmates,"*
* The John Howard and Elizabeth Fry societies
* Colleges that offer training for prison guards\
  *Mohawk and Niagara Colleges, for example*
* Construction companies and closed-shop construction unions who will be hired by the big companies that win the contracts\
  *It is good to pressure companies before contracts are awarded, since there are often costly penalties for withdrawing.*
* Architects, environmental assessment firms, consulting companies, lawyers and other professionals who will be hired by the companies that win the contracts\
  *It can be hard to figure out exactly who is involved in this work, but it seems like Ernst & Young LLP has been involved in consulting around the Thunder Bay Correctional Complex, presumably hired by Bird Construction. It is much easier to access this information if you are a member of a construction association or a trade union.*
* The Council of Ontario Construction Associations (COCA) and other construction lobby groups
* Construction companies who are submitting proposals during the RFQ stage
* The Ontario Liberal and NDP parties
* Construction companies that are awarded contracts\
  *Ellis Don\
  Bird Construction*
* The Ontario Public Sector Employees Union (OPSEU)\
  *Warren Thomas: "This announcement puts yet more resources in the corrections system. I'm particularly happy to see the new infrastructure improvements. But the proof is in the pudding, and I'll be even happier when I see shovels in the ground. These projects can't be completed soon enough."*
* The Ontario Progressive Conservative Party\
  *Premier Doug Ford: "By making these important investments in Eastern Ontario, we will upgrade our corrections infrastructure, better protect our correctional officers, and contribute to our economic recovery through these new construction projects."*
* OPSEU's Corrections Division
* The Ministry of the Solicitor General

##### Direct Action Targeting the Construction Process

The Northern and Eastern prison expansion strategies involve work on five sites spread across a large area over many years. Each of these projects will go through phases of planning, consultation, environmental assessment, and bidding before construction actually begins. The construction of each site will involve many different contractors and trades working together.

Most of these steps have to happen in a particular order, meaning a delay to one has a knock on effect, delaying all the others. A construction permit won't be issued until the environmental assessment is done, and foundations will have to be poured before modular units can be brought in. As well, many of the same companies will be involved in each project, meaning a delay to one is likely to create a delay to others.

Rather than trying to stop the project with one big push, we suggest creating many delays throughout the process. In particular, look for steps in the process that are chokepoints, such as mandatory site visits or concrete pours, where any disruption will be much more effective. These delays will stack on each other, drive up the cost of the project, and push it across multiple provincial governments.

Direct action means different things to different people, but we feel like it is worth using "direct action" in its more specific sense here. That means taking actions that directly stop the thing you don't want to happen from happening, as opposed to actions designed to get someone else to do it. Here are some examples:

Suppose there is a public consultation meeting about a proposed prison's request for a permit. The permitting process can't move ahead if that meeting doesn't happen, so rather than trying to get the committee to deny the permit, direct action means disrupting the meeting. This could look like filibustering the meeting so that it goes over time and has to be rescheduled, and it could also mean blocking the doors.

Direct action is certainly not the only way of creating delays, and we don't expect everyone who cares about stopping prison expansion to want to use direct action tactics. But direct action does not need to be very escalated or risky to be effective, so thinking about how to make your organizing more direct is still worthwhile. Taking actions that directly disrupt the construction process is the surest way of creating delays, and publicizing them also gives more opportunities to dramatize the issue, forcing groups to pick sides.

Understanding the campaign against prison expansion as having a crucial direct action component is important, since the supporters of the expansion will try to do to us the same thing we try to do to them: they will attempt to separate the more radical people using the more disruptive tactics from those they consider reasonable. They will try to recuperate -- they will invite the reasonable ones in for a seat at the table and use the police to target those who won't compromise.

Direct action and solidarity go together. Let's try to build a campaign that is diverse in its approach and decentralized in its structure, one that is resistant to being disrupted by repression and is capable of taking actions that slow these projects down.

[Previous Section: Case Study: Toronto South Detention Centre](https://escapingtomorrowscages.org/texts/4case-study)
