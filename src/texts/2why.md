---
subTitle: "" 
title: "Why Oppose Ontario's Prison Expansion?"
img: "/assets/images/projects/icon2.jpg"
linkText: "Read more"
---
First, briefly, **why oppose prisons** in general?

We take as our starting point that the world would be better without prisons. Prisons don't solve social problems, they exacerbate existing inequalities and play a crucial role in a violent capitalist system. The lie told about prisons is that by disappearing people who have been convicted of criminalized activities, our communities will be "safer". In reality, breaking up communities, only to later release people with more trauma and fewer resources, does far more harm.

Unsurprisingly, those dealing with poverty, addiction, or mental health struggles are vastly over-represented prison populations. Prisons frame these struggles as individual choices instead of symptoms of a capitalist system that disposes of those viewed as having less to contribute. The legal system itself is designed so that those with less resources are far more likely to end up incarcerated. The over-representation in prison of Indigenous, Black and other people of colour - all communities dealing with higher levels of surveillance and criminalization - is also indicative of how prisons entrench existing inequalities. In Canada, a country built on stolen Indigenous land, prisons have always been institutions used to lock up those who threaten the colonial state and the capitalist system it relies on.

For these reasons, the struggle against prison is a crucial part of struggles against other forms of oppression. Prison is one of the most explicit and violent ways that the state keeps the oppressed in their place and maintains the status quo.

### **Responding to Ontario's Prison Expansion Narrative**

Over the years, through moments of crisis/tension, the government has responded to crises in prison management by directing resources towards expanding the prison system in Ontario. Looking at public discourse around prisons over approximately the last decade, we can see a narrative that the provincial government has built up around their current prison expansion program.

Funding for the two largest projects, the Kemptville and Thunder Bay prisons, was approved back in 2017.

Around that time, Ontario prisons had come under scrutiny for their heavy-handed use of segregation, meaning solitary confinement. In one high-profile case, Adam Capay, a man from Lac Seul First Nation, was held in segregation over 1,500 days, much of that time in a cell that was brightly lit for 24 hours a day. His case was a starting point for two significant reports: *Out of Oversight, Out of Mind*, a report by the Ontario Ombudsman on the Ministry of Community Safety and Correctional Services' handling of segregation placements, and an interim report on from Ontario's Independent Advisor on Corrections Reform, also on the use of segregation.

In a press release, the province describes the reports as guiding the government's "ongoing work to reform Ontario's correctional system."

The work included "[chang](https://news.ontario.ca/mcscs/en/2016/10/ontario-to-begin-overhaul-of-the-use-of-segregation-in-the-province.html)[ing] segregation practices, as well as [investments](https://news.ontario.ca/mcscs/en/2016/12/ontario-hiring-more-staff-and-enhancing-supports-to-reform-correctional-services.html?_ga=1.8067061.617910489.1468957729) made to increase staff and mental health supports for those in custody."

Crucially, the press release also announces approved funding for a 325-bed multi-purpose correctional centre to replace the existing Thunder Bay Jail and Thunder Bay Correctional Centre, and a 725-bed multi-purpose correctional centre to replace the existing Ottawa-Carleton Detention Centre (the details of these projects have changed since then). These investments are described as increasing capacity and reducing overcrowding.

If the link between locking less people in segregation and building new prisons feels murky, the release goes on to explain that these actions meet the goals of reducing segregation by "building a system in which appropriate alternatives to segregation are more available for vulnerable inmates, such as pregnant women and those with acute mental health issues, and ensuring that segregation is used only in rare circumstances". If it still feels murky, perhaps it's because surely there are easier ways to avoid locking people in segregation than building new facilities.

While it already feels clear that the government can spin any crisis in corrections into an excuse to expand the prison system, let's look at a few other issues unfolding at the time.

In August 2017, a joint coroner's inquest was announced to investigate at least 8 overdose deaths in the Hamilton-Wentworth Detention Centre between 2012-2016. This is one particularly stark example of the overdose crisis unfolding inside Ontario prisons.

The report produced 62 recommendations, none of which were binding, but many of which provide possible justification for the expansion of prison infrastructure. For example, recommendations included introducing full-body scanners, limiting the number of prisoners to a cell, and housing new prisoners in a separate intake area.

Finally, in early 2016, the government of Ontario narrowly avoided a strike during contract negotiations with correctional officers (prison guards) and probation officers represented by Ontario Public Service Employees Union (OPSEU). Results of the negotiations included an end to a hiring freeze and the appointment of at least 25 new probation and parole officers. Understaffing and overcrowding of facilities has been a perennial talking point for the corrections unit of the union.

It is unsurprising that correctional staff complaints also throw support behind a prison expansion program. OPSEU president Warren Thomas has made multiple statements in wholehearted support of the Kemptville and Thunder Bay prison projects. In fact, he cited new prison construction as a reason corrections staff remained satisfied with OPSEU representation when complaints emerged from some union members in 2020.

All these events helped form the context for approving upwards of $500 million for prison expansion.

It is true that prisons in Ontario are horrifyingly overcrowded, poorly maintained, frequently locked down due to short staffing and lack even basic services like medical care. While prison expansion is far from the only answer to these problems, it's unsurprising that it is the one the government reached. Any crisis in corrections will be responded to by an expansion of the system, escalated forms of control, and further categorization, separation, and isolation of prisoners.

In addition to supposedly addressing issues like overcrowding, new projects claim to include more specialized services and programming. The Northern expansion strategy is billed as being "responsive to the needs of Indigenous people and communities", with "culturally appropriate spaces and aspects of the facilities." The Eastern strategy includes the expansion of the St. Lawrence Valley Correction and Treatment Centre, a facility specifically for those with mental health or developmental issues.

This ignores the fact that prisons perpetuate a cycle of harm against marginalized communities, and that more "culturally sensitive" or specialized forms of incarceration will never change this. There is also no guarantee that programming or specialized use of facilities will be permanent - the only guarantee is that the state's capacity to incarcerate people has forever expanded.

[Previous Section: Summary of the Projects](https://escapingtomorrowscages.org/texts/1summary/)

[Next Section: Fighting Recuperation](https://escapingtomorrowscages.org/texts/3recuperation/)

##### References

<https://news.ontario.ca/en/release/44567/ontario-taking-action-to-reform-correctional-system>

Ombudsman report: Out of Oversight, Out of Mind: <https://www.ombudsman.on.ca/resources/reports-and-case-summaries/reports-on-investigations/2017/out-of-oversight,-out-of-mind>

<https://news.ontario.ca/en/release/45931/joint-inquest-into-the-eight-deaths-at-the-hamilton-wentworth-detention-centre>

<https://www.thestar.com/news/gta/2021/01/07/bitter-fight-by-ontario-correctional-officers-to-form-own-union-fizzles.html>

<https://news.ontario.ca/en/release/60542/ontario-announces-successful-bidder-for-expansion-of-northern-correctional-facilities>

<https://news.ontario.ca/en/release/57233/ontario-investing-in-frontline-corrections-workers>



