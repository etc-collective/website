---
subTitle: "" 
title: "Summary of the Projects"
img: "/assets/images/projects/icon3.jpg"
linkText: "Read more" 
---

The current round of prison expansion in Ontario is divided into two areas: the Northern Strategy and the Eastern Strategy. Although there is a lot we could say about each of these prisons, our goal in this section is to provide you with all the details about the prison expansion projects as simply as possible. We will be getting into more history and context later.

Prison is being restructured across the province, and at this time the process is focused on northern and eastern Ontario. These two strategies follow on a previous wave of prison expansion, heavily focused on Toronto but that also included a new prison in Windsor. There will be other waves after this one, probably aimed at south-western Ontario.

Both of these strategies double the prison capacity in the areas they affect. Although some of the projects involve replacing older prisons with new ones, the older ones are also being renovated. Because they are very likely to continue being used in some form, until they are demolished, they still represent capacity that the provincial prison system has at its disposal.

As well, the capacity numbers mentioned here only refer to the official capacities indicated by the ministry for the prisons in question -- the phrase they use is "number of beds". Considering that these capacities have gone up with time (adding extra bunks) and that many prisons are over-capacity (people sleeping on the floor), the anticipated capacities listed for all this new construction are only the beginning.

If you look into these projects at all, you will see that the ministry is finding every excuse to talk about how much programming, yard space, cultural activities, treatment or whatever the prisons will also contain. But remember: uses come and go, while the infrastructure will remain.

It is easy to change how buildings are used - there are unused gyms, woodworking shops, kitchens, visiting spaces, libraries, and more all hiding unused within existing provincial prisons. We do not take the ministry seriously about its programming intentions – what we do take seriously is the number of bodies they intend to cage.

Let’s dig into the details and see what the Northern and Eastern Strategies involve. Our goal here is to see how the individual projects fit into a province-wide plan that can be opposed on that scale.This is not just for educational purposes, it is for stopping prison expansion and creating a world without prison. None of these projects are inevitable.

## Northern Strategy

### What is being built

There are two parts to the prison expansion being done in the North, building a new prison and expanding two old ones.

The main focus of the Northern Strategy is the construction of the **Thunder Bay Correctional Complex (TBCC)**, a new prison whose stated capacity is 325 people.

It is apparently being built to replace two existing prisons, the Thunder Bay Correctional Centre and the Kenora Jail. Their combined capacity is 280 people. However, **both of these prisons are being expanded** “to address overcrowding during construction of the TBCC.” Their capacity is being increased by 50 through these renovations, and they also claim to be adding “programming space” and a “cultural yard.”

In total then, space for 375 additional prisoners is being added in northern Ontario. The province is claiming that the existing prisons will be closed when the new one is completed, and that may well be true. But it is also quite possible that the older buildings will be simply repurposed. Already with the expansions, the capacity of the existing facilities adds up to 330, while the capacity of the new facility is 325, and it seems unlikely that the province will accept a net reduction in capacity.

(*Update as of January 2023:* As expected, the capacity of the TBCC has been increased on the Infrastructure Ontario website from 325 to 345. The capacity of the additions to the Thunder Bay Correctional Centre has also increased to 50 – this was previously the combined capacity of the correctional centre and the Kenora Jail. The Province has also confirmed that the expansion to the Thunder Bay Correctional Centre will be permanent, and connected to the new TBCC. See the [January 2023 update](https://escapingtomorrowscages.org/news/updates-jan-2023/) for more details.) 

### What is the status?

The Northern Strategy is much further along than the Eastern strategy.

##### Kenora Jail and Thunder Bay Correctional Centre

The contracts for the expansions of the Kenora Jail and the Thunder Bay Correctional Centre were given to [Bird Construction](https://www.bird.ca/scp) on March 4, 2021, using an expedited process. Bird is partnered with a company called [Stack Modular](https://www.stackmodular.com/). The additions are modular, so are being built off-site (likely at Stack’s factory in China) and then will be shipped to Thunder Bay.

What does it look like to assemble prison cells on an assembly line? In February, Bird Construction posted [this video on Twitter](https://twitter.com/builtbybird/status/1496862587769786374?cxt=HHwWjMC42fil9sUpAAAA).

On the Ministry of Justice side of things, the expansion is seemingly being overseen by [Daniel Kielly](https://ca.linkedin.com/in/daniel-kielly-pmp-05349814), who is a manager of Public-Private Partnerships. He brags about having been involved in building nine police stations and two prisons. He recently shared a video on LinkedIn of the modules being assembled.

##### **Thunder Bay Correctional Complex**

For the TBCC, Ellis Don Infrastructure Justice was just chosen to build this new prison. Let’s briefly look at the selection process that led to this decision, since we will be talking about it again later:

The province develops an outline for the project it wants and posts a Request for Qualifications (RFQ) on the Infrastructure Ontario website. Various companies respond to the RFQ and the province selects a short list. This short list is then invited to submit a more detailed proposal for how they would deliver the project during the Request for Proposals (RFP) stage. One of these companies is then selected to do the actual work.

The RFP stage for the TBCC launched about a year ago, and Ellis Don Infrastructure Justice and Fengate PCL Progress Partners were invited to participate -- unsurprisingly, these two companies worked together on the Toronto South. In April 2022, Ellis Don was awarded the contract.

Infrastructure Ontario's January 2022 Market Update says they expect the project to be "in execution" by June 2022 and completed by summer 2024.(1)

## Eastern Strategy

The Eastern Strategy involves building two new prisons and upgrading three others. In total, it increases prison capacity by 375.

The new prisons:

The Eastern Ontario Correctional Complex, or the **Kemptville Prison**. It will be built on provincially-owned land in Kemptville, south of Ottawa, on the site of a former agricultural college, and is intended to incarcerate 235 people. In theory, it is to replace the existing provincial prison in Ottawa, the Ottawa-Carleton Detention Centre (OCDC), but it is not a given that this will occur, as there is also talk of renovating OCDC for other uses.

The **Brockville Correctional Complex** will be built on the same site as the existing St. Lawrence Valley Correction and Treatment Centre (SLVCT) in Brockville (more on the SLVCT later). It replaces the Brockville Jail, which is currently back in operation after having been closed due to a covid outbreak. Its anticipated capacity is 66, a 50% increase from the current 44.

The expanded prisons:

The **Quinte Detention Centre** in Napanee is being expanded, with a separate building being constructed on the same site to house a 48-person women's unit.

The **St. Lawrence Valley Correction and Treatment Centre** (SLVCT) is being expanded with a new facility targeting women, with capacity for 25 more prisoners. There is currently only a men's facility with a 100-person capacity. The new building is expected to be built north-west of the existing prison on the same site.

The SLVCT is one of a small number of provincial prisons reserved for prisoners with mental health or developmental issues who have been referred by other institutions. There are forensic psychology units scattered in hospitals around Ontario (both Hamilton's St. Joseph's, Kingston's Providence Care and Oshawa's Ontario Shores have all seen their psych prisons expand in recent years), but SLVCT is one of the very rare stand-alone psychiatric prisons. The other one we are aware of was Oak Ridge, in Penetanguishene, which closed in 2014, just before a judge ruled that prisoners there had been subjected to torture in the 70s and 80s.

Finally, once the Kemptville Prison is complete, the Ottawa-Carleton Detention Centre (**OCDC**) will be renovated and repurposed. This project is a big question mark, but the fact that OCDC is not being torn down means that all of the prison cells being built in Kemptville are new capacity.

### What is the status?

Of all of these projects, only the Kemptville Prison has had any progress, and it is in the earliest stages. It is expected that the RFQ will be issued at some point between July and September 2022, according to the January Market Update (1), and work will not begin until 2024.

There has been some preliminary survey and field work on the site in fall 2021, but otherwise, College Rd., just east of County Rd. 44 in Kemptville, is quiet. All the old agricultural college buildings are still there, boarded up...The most notable development around this project so far is the opposition to it that has emerged – check our resources page for more details.

The Brockville Correctional Complex and the women's facility at the SLVCT are seemingly being handled together, and the RFQ is expected there in October to December of 2022 for a start date in 2025. For the Quinte Detention Centre expansion, the RFQ is not expected until April or June 2023 with a start date in 2024.

<img 
src = "/assets/images/on-all-labels2.png"
alt = "Map of northern ontario prisons">

<img 
src = "/assets/images/on-south-labels.png"
alt = "Map of southern ontario prisons">

[Next Section: Why Oppose Ontario's Prison Expansion?](https://escapingtomorrowscages.org/texts/2why/)

# References

1. <https://infrastructureontario.ca/January-2022-Market-Update/>
2. <https://www.ontario.ca/page/correctional-facilities>

