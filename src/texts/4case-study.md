---
subTitle: "" 
title: "Case Study: Toronto South Detention Centre"
img: "/assets/images/projects/icon1.jpg"
linkText: "Read more"
---
The Northern and Eastern Strategies are not the first waves of prison expansion in Ontario (and they won't be the last). They directly build on the previous wave, which was focused on Toronto. The Toronto strategy closed older prisons (Metro West, the Don Jail, and Mimico) and created the Toronto South Detention Centre (TSDC). Described when it opened as "the model for institutions like this for years to come," a close look at the TSDC will help us know what to expect from the new prisons.

The older prisons were more cramped, with much smaller ranges, fewer or no programming spaces, and guard areas that were close to prisoner areas, often just with bars in between them allowing sound to pass. The conditions in these facilities were widely condemned, and the provincial prison system has been under pressure from many sides to change them. Hamilton's Barton Jail and London's EMDC are prisons of this type that are still in operation.

Starting in the late 1990s, these have been getting replaced by "pod prisons," typified by Central North/Penetang, Central East/Lindsay, and Maplehurst. The pod prisons originate in a tough-on-crime approach from the 1990s when the system was preparing to lock up more people for longer. They are designed with two main considerations: cutting prisoners off from the community and saving money. Pod prisons are usually big and are built far outside of cities, and their structure allows for fewer guards to watch more prisoners, keeping costs down.

The TSDC is a pod prison, but coming almost 20 years after Maplehurst, it was also meant to be an updated version, reflecting modern ideas about how control and incarceration should happen. Let's look at pod prisons in general and then at the TSDC in particular.

This type of prison is divided up into pods which are in turn divided up into ranges. Each pod is a world unto itself, with its own visiting areas, yard, programming space, and offices, allowing the prison to use and tailor each unit differently.

The prison system initially framed this move towards cutting different prisoners off from each other in terms of reducing gang violence. But they quickly used it to create areas for Protective Custody (PC), mental health and medical areas, and separate work or school ranges. It was also used to make racial segregation easier, under the pretext of controlling gang activity. Prisoners were sorted into different areas, which could then be treated differently, either awarded privileges or punished, or even pitted against each other

Each pod is typically circular and contains six or more ranges that face onto a central guard area. The guards are separated from the prisoners by plexiglass at all times unless they are actively patrolling a range -- this is one of the pod prison's defining features. In the centre of the guard area is an enclosed, elevated surveillance hub, often called the bubble, where a single guard watches all the cameras and - crucially - controls all the door locks. This structure allows a smaller number of guards to watch more prisoners, since a single team of guards is now watching six ranges instead of one or two (one result of this is that a single guard absence can lead to the lockdown of more ranges).

Pod prisons are typically quite spacious relative to the older prisons, with high ceilings, large ranges, and usually at least one programming room per pod. They are more likely to have programs, such as libraries, even if access to these things will depend heavily on where you land within the prison.

By the 2010s the world had moved on (at least in its rhetoric) from the nineties war on crime. Like every prison expansion, the TSDC was framed in part as a response to criticism of the prison system at the time it was built.

All prison is about creating separation, and the TSDC intensified this. For instance, it uses video visits, instead of visits through glass. This is quite explicitly about control, since you can still get a visit through glass, but it is now an earned privilege.

Maplehurst, Lindsay, and Pentang are out in the country, which isolates prisoners from their community.The TSDC is in the city, but compensates for this by frosting all the windows so that prisoners cannot see out, cutting them off just as effectively. Previously, this was only done in women's prisons, where it is apparently meant to prevent harassment. The yards in the other pod prisons were already just concrete rooms with a chain-link ceilings, but TSDC went a step further and completely closed in the “yard” with concrete, providing vents for fresh air.

In these ways, the TSDC is an intensified version of the other pod prisons, but one crucial difference is the idea of "direct supervision." This refers to reducing the amount of space between guards and prisoners by moving the guard stations into the prisoners areas. The goal is that there will be more interactions between prisoners and guards and that this will lead to the institution having more control in what happens on ranges (which the system frames as "reducing violence").

A large majority of the units in the TSDC were meant to be direct supervision units, and being on one of these units (as opposed to being kept on more restrictive intake, seg or behavioural units) is a condition for accessing through glass visits and other privileges.

Direct supervision is a way of separating prisoners from each other. By inserting a guard into more aspects of prison life, those who are willing to get along with guards will access more privileges, while those who won't cooperate are more easily singled out. This undermines solidarity and makes prisoners more dependent on their jailers and less on each other. 

From the perspective of the system, TSDC has so far been a botched project. Its direct supervision structure has not been implemented and it operates in the same way as a classic pod prison. However, there is currently a massive hiring wave of new guards (including partnerships with places like Mohawk and Niagara Colleges), so the uses of the infrastructure may change.

The infrastructure matters more than the use though. TSDC is an example of how the system can sell new prison construction with supposedly progressive uses, but once they have the walls and cages, they have sole discretion over how they are used. 

[Previous Section: Fighting Against Recuperation](https://escapingtomorrowscages.org/texts/3recuperation)

[Next Section: A Strategy for Stopping Prison Expansion](https://escapingtomorrowscages.org/texts/5strategy)

##### References

A Tour of the Toronto South Detention Centre: <https://torontoist.com/2013/10/a-tour-of-the-new-toronto-south-detention-centre/>

OHRC Report on Conditions of Confinement at the Toronto South Detention Centre: <https://www.ohrc.on.ca/en/report-conditions-confinement-toronto-south-detention-centre>
