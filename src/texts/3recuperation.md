---
subTitle: "" 
title: "Fighting Against Recuperation"
img: "/assets/images/projects/icon4.jpg"
linkText: "Read more"
---
Although we have many reasons to be against prisons and against this prison expansion project in particular, our motivations are profoundly anti-capitalist, since prison is an important part of how capitalism functions. However, we understand that not everyone will share this motivation, even if we have a shared goal of keeping this prison expansion project from happening. Therefore, while our primary goal is abolition, our secondary goal is to stop these prisons from being built, so we will need to find and reinforce common ground with others. Perhaps the best foundation from which to work is to identify the state, in the form of the provincial government, as a common adversary in this fight.

The state will try to undermine any anti-expansion organizing taking place, and the best way for it to do that is to take up a version of our demands and use them to justify expansion and reform; in a word, recuperation.

Recuperation is when the state (or another powerful body) tries to involve its critics in a process of transforming the institutions they criticize. For this expansion project specifically, we see the prison system taking up arguments used against it (like overcrowding and the absence of programs) and inviting its critics to support them in changing certain details of the system, obviously stopping short of changing anything that would challenge their power. It gives us, the critics, an air of control, like we were able to facilitate change. When in reality, nothing about the system is changed. The state is still locking people in cages, they just happen to be cages with different empty promises attached.

The following are a couple examples of recuperative actions taken by the Province of Ontario in response to critiques of the Ontario prison system in recent years. The point  we want to make with them is that the prison system managed to use a  narrow concession to stop specific campaigns targeting conditions inside.

There was an inspiring and successful struggle that emerged from inside prisons around Bell's collect call system. The prison system eventually decided to react, but it ignored the more general critiques about the effects of cutting prisoners off from their supports and focused on the narrow demand of being able to place direct calls to cell phones. They did this by awarding the phone contract to Telmate, a Texas-based prison contracting company. Prisoners can now make direct calls to cell phones, but issues of surveillance, number blocking, and unequal access remain, and the system  also pivoted towards video visits and scanning mail, creating new obstacles to comunication.

Similarly, in the mid-2010s there was a mass movement among prisoners to get access to better meals (centered around accessing the kosher diet). This involved coordinated action by hundreds of prisoners and eventually a successful legal challenge. It led to significant reform to the menus in provincial prisons and also to how decisions around special diets are made. Many, many more prisoners have access to special diets now and there are fewer completely inedible meals, but the experience of incarceration has not become any healthier.

Please don't hear this as an argument against trying to help prisoners win specific demands or that the campaigns around food and phones were not important and successful. Supporting inside struggle is a big part of what we do too. 

At this moment the provincial government is using some key demands from the past decade of struggle to justify building new prisons, and we think an analysis of recuperation can help us stop them.

The state will play on concerns about the brutal treatment of prisoners with mental health issues to justify building new prisons by calling them treatment centres. They will use cultural programming and healing lodges to justify building new prisons that will be disproportionately filled with Indigenous people. They will claim to be addressing overcrowding while making room to lock more people in cages.

The above examples involve small pieces of overall systemic brutality of the prison industrial complex. Prisoners are some of the most forgotten people in our society. Prison exists to erase certain humans, and it does its job well. Although forcing small concessions, like direct calls to cell phones, can feel good and make people's time easier, we all know that is not a challenge to the prison system. It is simply slightly less torture, and to respond to the current wave of recuperation and prison expansion, we need to confront that reality. If our goal is abolition, we need to not get lost in the state's language of reform and start aiming bigger.

[Previous Section: Why Oppose Ontario's Prison Expansion?](https://escapingtomorrowscages.org/texts/2why/)
[Next Section: Case Study: Toronto South Detention Centre](https://escapingtomorrowscages.org/texts/4case-study/)
