---
layout: "layouts/homepage.njk"
hero:
  subTitle: ""
  title:
    firstPara: "Escaping Tomorrow's Cages"
    secondPara: ""
  seeWorkBtn:
    text: "See Our Works"
    link: "#project"
  connectBtn: 
    text: "Connect with us"
    link: "contact"

portfolio:
  subTitle: "COMING SOON"
  title: ""
  seeAllbtn:
    link: "portfolio"
    text: "MORE WORKS"

title: Escaping Tomorrow's Cages 
featureImg: "/assets/images/projects/project-thumb-one.jpg"
---

*Escaping Tomorrow's Cages* is written in absolute opposition to the existence of prisons. The Province of Ontario is in the midst of an initiative to expand the provincial prison system in eastern and northern Ontario, and our hope is for this text to contribute to a struggle capable of stopping it.

The initiative includes expanding several existing prisons and building at least two new ones. Together, these projects will effectively double the provincial prison capacity in the areas it effects, mainly in women's prisons. We find any expansion of the prison system unacceptable.

In the north, existing prisons in Kenora and Thunder Bay are being expanded as we speak, and work on a new, larger prison in Thunder Bay is slated to begin this summer. In the east, one prison in Brockville is being expanded and a new one is being built, the prison in Napanee is expanding, and a large new prison is being built in Kemptville, south of Ottawa. 

This is an invitation to get familiar with the projects and to start dreaming of ways to oppose them. We also invite readers to look at this wave of expansion less as local construction projects and more as part of a province-wide struggle. Regardless of where you live in (or near) Ontario, all of these expansion projects affect you.

Every round of prison expansion is a chance for the state to respond to its critics. The Ontario prison system has faced some pointed criticism in the past decade: the widespread use of segregation, overdose deaths, and anti-Indigenous racism, to name a few. The Province is using all of these to help build a case for prison expansion, while we argue that expanding a rotten system won't improve conditions and certainly does not move us towards a world without prison.

Once a prison is built, it cannot be undone. No amount of reform will ever make them into anything other than what they are: buildings that hold cages for humans. The current expansion program relies heavily on rhetoric about "improving" and "reforming" prisons: new culturally specific programming, a solution to overcrowding, facilities designed to aid with issues pertaining to mental health and addiction. But the bars and walls will outlast the funding and political will for these programs, just like they have in the past.

These structures must not be built. Our hope is to spread opposition to Ontario's prison expansion as widely as possible. We see this an opportunity to advance anti-prison politics throughout our region, and maybe even stop some prisons being built along the way.

We will be publishing this essay in six parts throughout May 2022, starting on May 1st, International Workers Day. The second part, Summary of the Projects, will provide details about each of the projects and its current status. 

Introduction\
Summary of the Projects\
Why Oppose Ontario's Prison Expansion?\
Fighting Against Recuperation\
Case Study: Toronto South Detention Centre\
A Strategy for Stopping Prison Expansion
